# Anleitung zu Einrichten des NodeJs Servers auf dem Raspberry PI

## NodeJs auf dem Pi installieren
- über die Konsole oder ssh auf den Pi anmelden
- in der Konsole folgende Befehle ausführen (gegebenfalls mit sudo):
- - `sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`
- - `sudo apt-get upgrade`
- - `sudo apt-get install -y nodejs build-essential`
> Quelle: "https://crycode.de/installation-von-node-js"

# Git installieren
> `sudo apt install git`

## Repo ausschecken
- `cd /var/www/`
- `sudo git clone https://gitlab.com/123kicker/123kicker.git`
- `cd 123kicker`
- `sudo npm i`

## Server einrichten
> https://nodejs.org/en/docs/guides/getting-started-guide/

## Server starten
> `node app.js`

## AutoStart einrichten
> https://www.instructables.com/id/Nodejs-App-As-a-RPI-Service-boot-at-Startup/
- Path und Namen anpassen