module.exports = {
    "roots": [
        "<rootDir>/BackEnd/src"
    ],
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
}