import {Game} from "./game";

let game: Game;
const expectScore = (player1Score: number, player2Score: number): void => {
    expect(game.getPlayer1Score()).toEqual(player1Score);
    expect(game.getPlayer2Score()).toEqual(player2Score);
};
const player1ScoredNTimes = (goals: number): void => {
    for (let iter: number = 0; iter < goals; iter++) {
        game.player1Scored();
    }
};
const player2ScoredNTimes = (goals: number): void => {
    for (let iter: number = 0; iter < goals; iter++) {
        game.player2Scored();
    }
};
beforeEach((): void => {
    game = new Game();
});

test('givenNewGame_ScoreShouldBe_0:0', (): void => {
    expectScore(0, 0)
});

test('givenPlayer1Scored_ScoreShouldBe_1:0', (): void => {
    player1ScoredNTimes(1);
    expectScore(1, 0);
});

test('givenPlayer2Scored_ScoreShouldBe_0:1', (): void => {
    player2ScoredNTimes(1);
    expectScore(0, 1);
});

test('givenPlayersScoredMultipleTimes_ScoreShouldBeCorrect', (): void => {
    player1ScoredNTimes(2);
    expectScore(2, 0);
    player2ScoredNTimes(2);
    expectScore(2, 2);
});

test('givenBothPlayerScored5Times_GameIsNotFinished', (): void => {
    player1ScoredNTimes(5);
    player2ScoredNTimes(5);
    expect(game.isFinished()).toEqual(false);
});

test('givenPlayer1Scored10Times_GameIsFinished', (): void => {
    player1ScoredNTimes(10);
    expect(game.isFinished()).toEqual(true);
});

test('givenPlayer2Scored10Times_GameIsFinished', (): void => {
    player2ScoredNTimes(10);
    expect(game.isFinished()).toEqual(true);
});

test('givenPlayer1Scored10Times_Player1Won', (): void => {
    player1ScoredNTimes(10);
    expect(game.didPlayer1Win()).toEqual(true);
});

test('givenPlayer1Scored9Times_Player1DidNotWin', (): void => {
    player1ScoredNTimes(9);
    expect(game.didPlayer1Win()).toEqual(false);
});

test('givenPlayer2Scored10Times_Player2Won', (): void => {
    player2ScoredNTimes(10);
    expect(game.didPlayer2Win()).toEqual(true);
});

test('givenPlayer2Scored9Times_Player2DidNotWin', (): void => {
    player2ScoredNTimes(9);
    expect(game.didPlayer2Win()).toEqual(false);
});


