export type Score = {
    Player1: number,
    Player2: number
};

export class Game {
    private Player1Score: number;
    private Player2Score: number;

    constructor() {
        this.Player1Score = 0;
        this.Player2Score = 0;
    };

    public GetScore = (): Score => ({
        Player1: this.getPlayer1Score(),
        Player2: this.getPlayer2Score()
    })

    public getPlayer1Score = (): number => {
        return this.Player1Score;
    };
    public getPlayer2Score = (): number => {
        return this.Player2Score;
    };
    public player1Scored = (): void => {
        this.Player1Score++;
    };
    public player2Scored = (): void => {
        this.Player2Score++;
    };
    public isFinished = (): boolean => {
        return this.didPlayer1Win() || this.didPlayer2Win();
    };
    public didPlayer1Win = (): boolean => {
        return this.getPlayer1Score() >= 10;
    };
    public didPlayer2Win = (): boolean => {
        return this.getPlayer2Score() >= 10;
    };
}