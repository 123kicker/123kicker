"use strict";
exports.__esModule = true;
var Game = /** @class */ (function () {
    function Game() {
        var _this = this;
        this.GetScore = function () { return ({
            Player1: _this.getPlayer1Score(),
            Player2: _this.getPlayer2Score()
        }); };
        this.getPlayer1Score = function () {
            return _this.Player1Score;
        };
        this.getPlayer2Score = function () {
            return _this.Player2Score;
        };
        this.player1Scored = function () {
            _this.Player1Score++;
        };
        this.player2Scored = function () {
            _this.Player2Score++;
        };
        this.isFinished = function () {
            return _this.didPlayer1Win() || _this.didPlayer2Win();
        };
        this.didPlayer1Win = function () {
            return _this.getPlayer1Score() >= 10;
        };
        this.didPlayer2Win = function () {
            return _this.getPlayer2Score() >= 10;
        };
        this.Player1Score = 0;
        this.Player2Score = 0;
    }
    ;
    return Game;
}());
exports.Game = Game;
