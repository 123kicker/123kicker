import { Game, Score } from "./game";

import { Gpio } from "onoff";

export class TableSoccerGame {
    private Game = new Game();
    private onScore: (score: Score) => void;

    constructor(onScore: (score: Score) => void) {
        this.onScore = onScore;
        const PlayerOneTrigger = new Gpio(24, 'in', 'rising', { debounceTimeout: 10 });
        const PlayerTwoTrigger = new Gpio(23, 'in', 'rising', { debounceTimeout: 10 });
        PlayerOneTrigger.watch(this.PlayerOneTriggered);
        PlayerTwoTrigger.watch(this.PlayerTwoTriggered);
        this.onScore(this.Game.GetScore());
    }

    private PlayerOneTriggered = () => {
        this.Game.player1Scored();
        this.onScore(this.Game.GetScore());
    }

    private PlayerTwoTriggered = () => {
        this.Game.player2Scored();
        this.onScore(this.Game.GetScore());
    }
}

