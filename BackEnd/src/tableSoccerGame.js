"use strict";
exports.__esModule = true;
var game_1 = require("./game");
var onoff_1 = require("onoff");
var TableSoccerGame = /** @class */ (function () {
    function TableSoccerGame(onScore) {
        var _this = this;
        this.Game = new game_1.Game();
        this.PlayerOneTriggered = function () {
            _this.Game.player1Scored();
            _this.onScore(_this.Game.GetScore());
        };
        this.PlayerTwoTriggered = function () {
            _this.Game.player2Scored();
            _this.onScore(_this.Game.GetScore());
        };
        this.onScore = onScore;
        var PlayerOneTrigger = new onoff_1.Gpio(24, 'in', 'rising', { debounceTimeout: 10 });
        var PlayerTwoTrigger = new onoff_1.Gpio(23, 'in', 'rising', { debounceTimeout: 10 });
        PlayerOneTrigger.watch(this.PlayerOneTriggered);
        PlayerTwoTrigger.watch(this.PlayerTwoTriggered);
        this.onScore(this.Game.GetScore());
    }
    return TableSoccerGame;
}());
exports.TableSoccerGame = TableSoccerGame;
