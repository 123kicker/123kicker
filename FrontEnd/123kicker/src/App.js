import React, {Component} from 'react';
import './App.css';
import io from 'socket.io-client';


class App extends Component{
  state = {
    score: {
      Player1: 0,
      Player2: 0
    }
  };
  socket = io('http://localhost:88');
  componentWillMount(){
    const {socket} = this;
    socket.on('connect', () => {console.log("connected!")});
    socket.on('getScore', (data) => {this.setState({score: data})});
    socket.on('disconnect', () => {console.log("disconnected!")});
  }
  render() {
    const {socket} = this;
    return ([
        <div className="App">
          <button
              onClick={()=>{socket.emit("scorePl1")}}
          >Player 1</button>
          <button
              onClick={()=>{socket.emit("scorePl2")}}
          >Player2</button>
        </div>,
        <div>
          {this.state.score.Player1}
        </div>,

          <div>
            {this.state.score.Player2}
          </div>
        ]
    );
  }
}

export default App;
