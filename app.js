const express = require("express");
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const {TableSoccerGame} = require("./BackEnd/src/tableSoccerGame");

const Port = 88;

app.use(express.static('FrontEnd/123kicker/build'));
app.get('/', function (req, res) {
    res.sendFile('index.html');
});

server.listen(Port, function () {
    console.log(`Example app listening on port ${Port}!`);
});

io.on('connection', function (socket) {
    const TABLESOCCERGAME = new TableSoccerGame((score) => (socket.emit('getScore', score)));
});